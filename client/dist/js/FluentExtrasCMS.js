(function($) {
    $.entwine('ss', function($){

        var fluentConfig = function() {
            const section = 'TractorCow\\Fluent\\Control\\LocaleAdmin';
            if (window
              && window.ss
              && window.ss.config
              && window.ss.config.sections
            ) {
              const config = window.ss.config.sections.find((next) => next.name === section);
              if (config) {
                return config.solnetfluentextras || {};
              }
            }
            return {};
        };

        $('.cms-fluent-selector .cms-fluent-selector-locales').entwine({
            onadd:function(){
                this.syncColor();
            },
            onchange:function(){
                this.syncColor();
            },
            syncColor:function(){
                var i;
                var config = fluentConfig();
                var value = this.val();
                var body = $('body');

                // Skip if no locales defined
                if (typeof config.colorMap === 'undefined' || config.colorMap.length === 0) {
                  return;
                }

                // Skip if we've set a value that doesn't have a matching color
                if (!config.colorMap.hasOwnProperty(value)) {
                    return;
                }

                // Remove all existing color classes
                for (i in config.colorMap) {
                    body.removeClass(config.colorMap[i]);
                    this.removeClass(config.colorMap[i]);
                }

                // Add new color
                body.addClass(config.colorMap[value]);
                this.addClass(config.colorMap[value]);
            }
        });
    });
}(jQuery));
