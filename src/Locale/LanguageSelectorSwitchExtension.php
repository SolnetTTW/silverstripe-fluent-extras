<?php

namespace Solnet\Locale;

use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;

class LanguageSelectorSwitchExtension extends DataExtension
{
    private static $db = [
        'HideLanguageSelector' => 'Boolean',
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab(
            'Root.Main',
            $hideLanguageSelector = CheckboxField::create(
                'HideLanguageSelector',
                _t('SolnetFluentExtras.HideLanguageSelector_Title', 'Hide the language selector?')
            )
        );

        $hideLanguageSelector->setDescription(
            _t(
                'SolnetFluentExtras.HideLanguageSelector_Description',
                'Checking this box hides the language selector for this subsite.'
            )
        );
    }
}
