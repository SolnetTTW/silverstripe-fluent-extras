<?php

namespace Solnet\Locale;

use SilverStripe\ORM\ArrayList;
use SilverStripe\CMS\Model\SiteTree;
use TractorCow\Fluent\Model\Locale;
use Wilr\GoogleSitemaps\GoogleSitemap;

/**
 * Alters the output of the Google Sitemaps module to account for
 * one SiteTree sitemap per configured Locale.
 */

class FluentGoogleSitemap extends GoogleSitemap
{
    /**
     * The google site map is broken down into multiple smaller files to
     * prevent overbearing a server. By default separate {@link DataObject}
     * records are keep in separate files and broken down into chunks.
     *
     * For SiteTree objects, one 
     *
     * @return ArrayList
     */
    public function getSitemaps()
    {
        // Get the list of sitemaps
        $sitemaps = parent::getSitemaps();
        // Repeat the SiteTree once for each Locale
        $out = ArrayList::create();
        foreach ($sitemaps as $sitemap) {
            if ($sitemap->ClassName === $this->sanitiseClassName(SiteTree::class)) {
                foreach (Locale::getLocales() as $locale) {
                    $newSitemap = clone $sitemap;
                    $newSitemap->Page = $newSitemap->Page.'?l='.$locale->Locale;
                    $out->push($newSitemap);
                }
            } else {
                $out->push($sitemap);
            }
        }
        return $out;
    }
}
