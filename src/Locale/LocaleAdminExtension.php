<?php

namespace Solnet\Locale;

use SilverStripe\Core\Extension;
use SilverStripe\Core\Config\Config;
use TractorCow\Fluent\Model\Locale;

/**
 * Adds extra configuration into the ModelAdmin javascript config that is passed
 * to the CMS, enabling us to do a value=>color lookup via JS.
 */

class LocaleAdminExtension extends Extension
{
    /**
     * @param Array $clientConfig
     */
    public function updateClientConfig(&$clientConfig)
    {
        $colorOptions = Config::inst()->get('Solnet\Locale\LocaleExtension', 'color_options');
        $locales = Locale::get();

        // Prepare map of locale => color option (eg. "en_US" => "color-blue")
        $colorMap = [];
        foreach ($locales as $locale) {
            $colorMap[$locale->getLocale()] = $locale->Color;
        }

        $clientConfig['solnetfluentextras'] = [
            'colorMap' => $colorMap,
        ];
    }
}
