<?php

namespace Solnet\Locale;

use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Extension;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;

class LocaleExtension extends Extension
{
    use Configurable;

    private static $db = [
        'Color' => 'Varchar',
    ];

    /**
     * @config
     * The color options selectable for each Locale.
     * 'classname' => 'Label'
     */
    private static $color_options = [
        'color-red' => 'Red',
        'color-orange' => 'Orange',
        'color-yellow' => 'Yellow',
        'color-green' => 'Green',
        'color-blue' => 'Blue',
        'color-indigo' => 'Indigo',
        'color-violet' => 'Violet',
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->insertAfter(
            'URLSegment',
            $color = DropdownField::create(
                'Color',
                _t('SolnetFluentExtras.Color_Title', 'Colour'),
                $this->config()->color_options
            )
        );
        $color->setEmptyString(_t('SolnetFluentExtras.Color_Placeholder', '(Default colours)'));
        $color->setDescription(
            _t('SolnetFluentExtras.Color_Description', 'The Locale select box in the left-hand menu will change to this colour when editing this Locale.')
        );
    }
}
